from models.common import Model

from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
from sklearn.decomposition import PCA
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV

import joblib


class LinearWithPCA(Model):
    """
    Base class for the models SVMWithPCA and LogisticWithPCA

    Assumes that the pca, linear pipeline is defined in `self.model`
    Performs a cross-validation grid search over the parameters defined
    in `self.parameter_grid`.

    See the 2 implementations, SVMWithPCA and LogisticWithPCA, for
    reference
    """

    def flatten_images(self, images):
        return images.reshape((images.shape[0], -1))

    def fit(self, images, labels):
        """
        Train the model with cross-validation
        """
        gs = GridSearchCV(
            estimator=self.model,
            param_grid=self.parameter_grid,
            verbose=1,
            n_jobs=-1,
            cv=2,
        )

        flat_images = self.flatten_images(images)
        gs.fit(flat_images, labels)

        self.model.set_params(**gs.best_params_)

        self.model.fit(flat_images, labels)
        # fit the model with the best params

    def predict(self, images):
        return self.model.predict(self.flatten_images(images))

    def save(self, path):
        joblib.dump(self.model, path)

    def load(self, path):
        self.model = joblib.load(path)


class SVMWithPCA(LinearWithPCA):
    """
    This model applies dimensionality reduction using PCA, followed by a SVM
    for classification
    """

    name = "SVM_with_PCA"

    def __init__(self):
        self.pca = PCA(n_components=16, svd_solver="randomized", whiten=True)
        self.svm = SVC(C=0.1, gamma=0.1)
        self.model = Pipeline([("pca", self.pca), ("svm", self.svm)])
        # our model, PCA followed by an SVM

        self.parameter_grid = {
            "pca__n_components": [5, 10, 16, 20, 50],
            "svm__C": [0.01, 0.03, 0.1, 0.3, 1],
            "svm__gamma": [0.01, 0.03, 0.1, 0.3, 1],
        }
        # the parameter grid we will search across


class LogisticWithPCA(LinearWithPCA):
    """
    This model applies dimensionality reduction using PCA, followed by a 
    logistic multiclass regressor

    Almost identical to the SVM class
    """

    name = "Logistic_With_PCA"

    def __init__(self):
        self.pca = PCA(n_components=16, svd_solver="randomized", whiten=True)
        self.logistic = LogisticRegression(
            C=0.1, solver="lbfgs", multi_class="multinomial"
        )
        self.model = Pipeline([("pca", self.pca), ("logistic", self.logistic)])
        # our model, PCA followed by an SVM

        self.parameter_grid = {
            "pca__n_components": [5, 10, 16, 20, 50],
            "logistic__C": [0.01, 0.03, 0.1, 0.3, 1],
        }
        # the parameter grid we will search across
