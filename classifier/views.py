import os
import tempfile
import numpy as np

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework import status

from models.manager import load_all
import classifier
from classifier.preprocess import preprocess
from classifier.serializers import FileUploadSerializer


models = None


def lazy_load_models():
    global models
    if models is None:
        print("Loading models")
        path = os.path.join(os.path.dirname(classifier.__file__), "models")
        models = load_all(path)


class ListVersion(APIView):
    """
    List all versions
    """

    def get(self, request):
        lazy_load_models()
        return Response({"versions": list(models.keys())})


class Classify(APIView):
    """
    The main classifier API
    """

    parser_classes = (MultiPartParser, FormParser)
    serializer_class = FileUploadSerializer

    def post(self, request, version):
        lazy_load_models()
        if version not in models:
            return Response(
                {"error": "This version does not exist"},
                status=status.HTTP_404_NOT_FOUND,
            )

        file_ = request.data["file_"]
        img = preprocess(file_)

        prediction = models[version].predict(np.expand_dims(img, axis=0))[0]
        # expand_dims: predict a 'batch' of size 1

        return Response(dict(prediction=prediction))
