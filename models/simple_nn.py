from keras.models import Sequential, load_model
from keras.layers import Dense
from keras.utils import to_categorical

import numpy as np

from models.common import Model


class SimpleNN(Model):
    """
    Simple neural network model, with a single hidden layer

    Train using ADAM optimizer
    """

    name = "simple_NN"

    def __init__(self):
        self.hidden_sizes = [30, 70, 100, 150, 300, 700]
        self.hidden_size = 70
        self.model = self.make_model(self.hidden_size)

    def make_model(self, size_hidden):
        model = Sequential()
        model.add(Dense(size_hidden, activation="relu", input_shape=(28 * 28,)))
        model.add(Dense(10, activation="softmax"))
        model.compile(
            loss="categorical_crossentropy",
            optimizer="adam",
            metrics=["categorical_accuracy"],
        )
        return model

    def flatten_images(self, images):
        return images.reshape((images.shape[0], -1))

    def set_train_model(self, hidden_size, flat_images, one_hot_labels):
        """
        Sets and trains the current model, returning the last validation accuracy
        """
        self.model = self.make_model(hidden_size)
        history = self.model.fit(
            flat_images,
            one_hot_labels,
            batch_size=200,
            epochs=100,
            validation_split=0.1,
        )
        return history.history["val_categorical_accuracy"][-1]

    def fit(self, images, labels):
        """
        Train models over a range of different hidden sizes.

        Select the one that performs best under the validation data.
        """
        best_hidden_size = None
        best_accuracy = -1
        one_hot_labels = to_categorical(labels)
        flat_images = self.flatten_images(images)
        for hidden_size in self.hidden_sizes:
            validation_accuracy = self.set_train_model(
                hidden_size, flat_images, one_hot_labels
            )
            if validation_accuracy > best_accuracy:
                best_accuracy = validation_accuracy
                best_hidden_size = hidden_size

        self.set_train_model(best_hidden_size, flat_images, one_hot_labels)
        print(best_hidden_size, best_accuracy)

    def predict(self, images):
        probabilities = self.model.predict(self.flatten_images(images))
        return np.argmax(probabilities, axis=-1)

    def load(self, path):
        self.model = load_model(path)

    def save(self, path):
        self.model.save(path)
