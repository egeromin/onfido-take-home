"""
Integration tests - take a while
"""

import unittest
import tempfile

from models.common import DataLoader
from models.linear import LogisticWithPCA, SVMWithPCA
from models.simple_nn import SimpleNN


class TestModels(unittest.TestCase):
    def setUp(self):
        random_seed = 123
        self.train_x, self.train_y, self.test_x, self.test_y = DataLoader().load(
            random_seed
        )

    def test_models(self):
        models = [SVMWithPCA(), LogisticWithPCA(), SimpleNN()]
        for model in models:
            model.fit(self.train_x, self.train_y)
            accuracy = model.accuracy(self.test_x, self.test_y)
            print("{} accuracy: {}".format(model.name, accuracy))
            self.assertGreater(accuracy, 0.2)

    def test_save_load_svm(self):
        model = SVMWithPCA()
        model.parameter_grid = {
            "pca__n_components": [16],
            "svm__C": [1],
            "svm__gamma": [0.1],
        }  # cheaply mock so I get optimal params
        model.fit(self.train_x, self.train_y)
        with tempfile.NamedTemporaryFile() as tf:
            model.save(tf.name)
            model.load(tf.name)
            accuracy = model.accuracy(self.test_x, self.test_y)
            print("loaded SVM accuracy: {}".format(accuracy))
            self.assertGreater(accuracy, 0.7)

    def test_save_load_simple_nn(self):
        model = SimpleNN()
        model.hidden_size = [100]  # cheap mock, as above
        model.fit(self.train_x, self.train_y)
        with tempfile.NamedTemporaryFile() as tf:
            model.save(tf.name)
            model.load(tf.name)
            accuracy = model.accuracy(self.test_x, self.test_y)
            print("loaded SimpleNN accuracy: {}".format(accuracy))
            self.assertGreater(accuracy, 0.15)
