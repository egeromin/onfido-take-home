"""
Model 'manager' module, used for training and loading all models
"""
import os

from models.linear import LogisticWithPCA, SVMWithPCA
from models.simple_nn import SimpleNN
from models.common import DataLoader


def all_models():
    return [LogisticWithPCA(), SVMWithPCA(), SimpleNN()]


def train_all(base_path):
    train_x, train_y, test_x, test_y = DataLoader().load(123)
    for model in all_models():
        model.fit(train_x, train_y)
        path = os.path.join(base_path, model.name + ".model")
        model.save(path)
        print("Saved model {}".format(model.name))


def load_all(base_path):
    models = all_models()
    for model in models:
        path = os.path.join(base_path, model.name + ".model")
        model.load(path)
        print("Loaded model {}".format(model.name))

    return {model.name: model for model in models}
