"""
Code to preprocess an image: load it from disk, 
resize and make black and white, for use with the 
MNIST classifier
"""
import numpy as np

from PIL import Image
from skimage.transform import resize


def preprocess(image_file):
    img = np.array(Image.open(image_file))
    if len(img.shape) == 3:
        # convert to grayscale
        grayscale = [0.299, 0.587, 0.114]
        if img.shape[-1] == 4:
            grayscale.append(0.0)
        img = np.dot(img, np.array(grayscale)).astype(np.uint8)

    return resize(img, (28, 28))
