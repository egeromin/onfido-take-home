from abc import ABC, abstractmethod, abstractproperty

import tensorflow as tf  # for loading MNIST easily
import numpy as np


class DataLoader:
    """
    class for loading `num_to_load` random images from MNIST
    """

    def __init__(self, num_to_load=500, split=4 / 5):
        self.num_train = int(num_to_load * split)
        self.num_test = num_to_load - self.num_train

    def load(self, random_seed=None):
        if random_seed is not None:
            np.random.seed(random_seed)
        (x_train, y_train), (x_test, y_test) = tf.keras.datasets.mnist.load_data()
        train_rows = np.random.randint(x_train.shape[0], size=self.num_train)
        test_rows = np.random.randint(x_test.shape[0], size=self.num_test)
        return (
            x_train[train_rows],
            y_train[train_rows],
            x_test[test_rows],
            y_test[test_rows],
        )


class Model(ABC):
    """
    Base class whose interface all models should implement
    """

    @abstractproperty
    def name(self):
        pass

    @abstractmethod
    def fit(self, images, labels):
        pass

    @abstractmethod
    def predict(self, images):
        pass

    @abstractmethod
    def save(self, path):
        pass

    @abstractmethod
    def load(self, path):
        pass

    def accuracy(self, images, labels):
        predictions = self.predict(images)
        return np.mean(predictions == labels)
