# Onfido Take Home

This is a web API for classifying handwritten digits like MNIST.

The API is written in Django and Django Rest Framework


## Access Online

Currently a very unstable deployment is available at:

http://onfido-demo.chickenkiller.com:8000

You can access it there.

Please ignore the strange domain name chickenkiller.com: it is one of the domains freely available via freedns.afraid.org.

This deployment is unstable and insecure because it uses the Django development server, not
Apache or Nginx -- unfortunately, I didn't have time to do a proper deployment.


## Endpoints

There are 2 endpoints:

- `GET /versions/` . This outputs the available models that you can choose from.
  Currently:

```
{
    "versions": [
        "Logistic_With_PCA",
        "simple_NN",
        "SVM_with_PCA"
    ]
}
```

- `POST /classify/<version>/` where `<version>` is one of the 3 available
  models, for example `/classify/SVM_with_PCA/`. This requires the file to be
  passed as multipart form data, with name `file_`. For example:

```
curl -i -X POST -F file_=@file.jpg http://onfido-demo.chickenkiller.com:8000/classify/SVM_with_PCA/
```

## Issues with Online Version

Note: these are only issues with the onfido-demo.chickenkiller.com deployment,
not with the code itself.

- You might run into problem uploading large images -- try downsampling to
about 500x500 pixels first.
- The `simple_NN` version does not seem to be working on the
unstable cloud implementation I have provided - unfortunately I haven't had
time to debug this yet. It suspect it is also due to memory limitations on the
micro instance that I am running.

These issues shouldn't be there on a larger machine. You can try installing
locally to confirm this.


## Installation

Requirements:

- python3.5
- pip3
- virtualenv
- GNU Make

Create a new virtualenv with python3.5 and activate it. Then install the
requirements:


```
pip install -r requirements.txt
```

To start the development server, do:

```
make run
```


## Testing

- unittests: `make test`
- integration tests: `make integration`


## Training

Default trained models are in the `classifier/models` directory. To retrain
these locally, do: `make train`.


## Code structure

This code is a standard Django + Django Rest Framework app. The project name is
`backend`: so the settings and configuration are in the `backend` directory.

There is only 1 app called `classifier`. This directory contains both views:
the one for listing the available model versions, and the one to perform the
classification. The views are defined in `views.py`, tests in `tests.py`, etc.
The serializer defined in `serializers.py` is not necessary, but it is useful
because then the browsable API -- which displays by default if you open the API
in a browser -- has a button for file upload, which is handy for testing.

The `models` directory is the one with the code defining the models. There are
3 models, 2 linear and 1 simple neural network. All models are expected to
implement a standard interface, defined in `models.common.Model`. This
directory isn't a django app proper, but it's a module referred to by the
`classifier` app. As such, it has a slightly different structure: in addition
to the code for models (`common.py`, `linear.py` and `simple_nn.py`),
there is `tests.py`, for unittests, and `integration.py`, for
integration tests.


## Full Report

- see report.pdf for the full report!
