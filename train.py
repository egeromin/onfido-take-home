"""
Script to train all models
"""

from models.manager import train_all

if __name__ == "__main__":
    import sys

    if len(sys.argv) < 2:
        print("Usage: python manager.py <base_path>")
        print("---")
        print("Use this to train and save all models to <base_path>")
        sys.exit(1)

    base_path = sys.argv[1]
    train_all(base_path)
