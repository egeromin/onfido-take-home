test:
	python -m unittest models.tests
	python manage.py test

integration:
	python -m unittest models.integration

run:
	python manage.py runserver

train:
	python train.py classifier/models/

