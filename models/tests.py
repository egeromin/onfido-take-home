import unittest

from models.common import DataLoader, Model


class TestLoader(unittest.TestCase):
    def test_loader(self):
        loader = DataLoader()
        train_x, train_y, test_x, test_y = loader.load()
        self.assertEqual(train_x.shape, (400, 28, 28))
        self.assertEqual(train_y.shape, (400,))
        self.assertEqual(test_x.shape, (100, 28, 28))
        self.assertEqual(test_y.shape, (100,))
