"""
Serializer for the file upload

This is not necessary, but useful for the browsable API,
as then the browsable API has a 'file upload' button
"""

from rest_framework import serializers


class FileUploadSerializer(serializers.Serializer):

    file_ = serializers.FileField()
