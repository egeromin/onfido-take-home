import os

from django.test import TestCase
from rest_framework.test import APITestCase
import tempfile
import numpy as np
from skimage.io import imsave

from classifier.preprocess import preprocess
import classifier


class TestPreprocess(TestCase):
    def test_preprocess(self):
        black = np.zeros((500, 300, 4)).astype(np.uint8)
        black[:, :, 3] = 255
        with tempfile.NamedTemporaryFile(suffix=".png") as tf:
            imsave(tf.name, black)
            tf.seek(0)
            img = preprocess(tf)
        self.assertEqual(img.shape, (28, 28))
        self.assertEqual(img.sum(), 0)


class TestCalls(APITestCase):
    def test_upload(self):
        path_6 = os.path.join(os.path.dirname(classifier.__file__), "6.png")
        with open(path_6, "rb") as fh:
            data = {"file_": fh}
            resp = self.client.post(
                "/classify/Logistic_With_PCA/", data, format="multipart"
            )
        self.assertEqual(resp.data["prediction"], 6)

    def test_versions(self):
        resp = self.client.get("/versions/")
        self.assertEqual(
            set(resp.data["versions"]),
            {"simple_NN", "Logistic_With_PCA", "SVM_with_PCA"},
        )
