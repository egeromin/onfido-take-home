numpy==1.16.0
scipy==1.2.0
scikit-learn==0.20.2
tensorflow==1.12.0
joblib==0.13.1
Keras==2.2.4
Django==2.1.5
djangorestframework==3.9.1
scikit-image==0.14.2

